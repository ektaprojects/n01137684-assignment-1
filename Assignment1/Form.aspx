﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Assignment1.Form" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Handmade Jwellery</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <h1>Handmade jweller</h1>
            <label>Name:</label><asp:TextBox runat="server" ID="clientName" placeholder="Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Name" ControlToValidate="clientName" ID="validatorName"></asp:RequiredFieldValidator>
            <br /><br />
            <label>Gernder</label>
            <asp:RadioButton runat="server" Text="Male" GroupName="gender"/>
            <asp:RadioButton runat="server" Text="Female" GroupName="gender"/>
            <br /><br />
            <label>Phone:</label><asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="This is an invalid phone number"></asp:CompareValidator>
            <br /><br />
            <label>Email:</label><asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br /><br />
            <label>Address:</label><asp:TextBox runat="server" ID="deliveryAddr" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="deliveryAddr" ErrorMessage="Please enter an address"></asp:RequiredFieldValidator>
            <br /><br />
            <label>Delivery Date:</label><asp:Calendar runat="server" ID="deliveryDate"></asp:Calendar><br /><br />
            <label>Time</label><asp:TextBox runat="server" ID="deliveryTime" placeholder="Delivery Time (24 hour format)"></asp:TextBox>
            <asp:RangeValidator EnableClientScript="false" ID="hourscontrol_1" runat="server" ControlToValidate="deliveryTime" MinimumValue="1100" Type="Integer" MaximumValue="2200" ErrorMessage="Our hours are between 11am and 10pm"></asp:RangeValidator>
            <br /><br />
            
            
            <label>Select Item</label>
            <asp:DropDownList runat="server" ID="product">
                <asp:ListItem Value="neckpiece" Text="Neck Piece"></asp:ListItem>
                <asp:ListItem Value="earings" Text="Earings"></asp:ListItem>
                <asp:ListItem Value="bracelet" Text="Bracelet"></asp:ListItem>
            </asp:DropDownList>
            <br /><br />
            <label>Select colour: </label>
            <asp:CheckBox runat="server" ID="gold" Text="Gold" />
            <asp:CheckBox runat="server" ID="silver" Text="Silver" />
            <asp:CheckBox runat="server" ID="rosegold" Text="Rose gold" />
            <br /><br />
            <label>Quantity:</label>
            <asp:TextBox runat="server" ID="qty" placeHolder="1-5"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter quantity" 
                ControlToValidate="qty" ID="RequiredFieldValidatorQty"></asp:RequiredFieldValidator>
            <asp:RangeValidator runat="server" ControlToValidate="qty" Type="Integer" MinimumValue="1" MaximumValue="5" 
                ErrorMessage="Enter quantity between 1-5 "></asp:RangeValidator>
            <asp:ValidationSummary ID ="validationSummary" runat="server" HeaderText="Please fix the followings...." ShowMessageBox ="false" 
                FontColor ="red" Font-Italic="true"/>
            <br /><br />
            <asp:Button Text="submit" runat="server" />
            </div>
            <br />
            
    </form>
</body>
</html>